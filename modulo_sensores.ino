#include <ESP8266WiFi.h>
#include <espnow.h>

// Configuración de WiFi
#define WIFI_AP "Redmi 10A"
#define WIFI_PASSWORD "Sopaipillas con mostaza"

// Dirección MAC del receptor ESP8266
uint8_t slaveAddress[] = { 0x2C, 0x3A, 0xE8, 0x0B, 0x46, 0x4F };

// Sensores
const int pResistor = A0; // Sensor Luminoso
const int sensorMovimiento = D1; // Sensor PIR

struct RelayStatus {
  int encendido;
  int luminosity;
  int movimiento;
};
RelayStatus relayData;

void OnSent(uint8_t *mac_addr, uint8_t status) {
  Serial.println("Mensaje enviado a otro ESP");
}

void setup() {
  Serial.begin(115200);
  pinMode(sensorMovimiento, INPUT);
  pinMode(pResistor, INPUT);

  // Conectar a WiFi
  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // Iniciar ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error al inicializar ESP-NOW");
    return;
  }
  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_add_peer(slaveAddress, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
  esp_now_register_send_cb(OnSent);
}

void loop() {
  int movimiento = digitalRead(sensorMovimiento);
  int luminosidad = analogRead(pResistor);
  relayData.luminosity = luminosidad;
  relayData.movimiento = movimiento;

  // Si detecta movimiento, envía una señal para encender el relé
  if (movimiento == HIGH) {
    relayData.encendido = 1;
    esp_now_send(slaveAddress, (uint8_t *)&relayData, sizeof(relayData));
    Serial.print("Encendido");
    delay(3000); // Esperar 3 segundos antes de enviar la próxima señal
  } else {
    // Si no detecta movimiento durante 5 segundos, envía una señal para apagar el relé
    static unsigned long tiempoUltimoMovimiento = millis();
    if (millis() - tiempoUltimoMovimiento > 5000) {
      Serial.print("Apagado");
      relayData.encendido = 0;
      esp_now_send(slaveAddress, (uint8_t *)&relayData, sizeof(relayData));
      tiempoUltimoMovimiento = millis();  
    }
  }
}
